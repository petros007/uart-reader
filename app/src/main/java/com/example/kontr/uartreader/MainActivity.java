package com.example.kontr.uartreader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

public class MainActivity extends AppCompatActivity implements USBSerialListener {

    USBSerialConnector usbSerialConnector;
    TextView showString;
    EditText rxText;
    ImageView imageView;
    String strBase64 = "";
    Button btnShowImage;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        usbSerialConnector = USBSerialConnector.getInstance();
        rxText = findViewById(R.id.rxText);
        showString = findViewById(R.id.showString);

        rxText.setFocusable(false);

        imageView = findViewById(R.id.imageView);

//        RxTextView.textChanges(rxText)
//                .onErrorReturn(error -> "Empty result")
//                .debounce(5, TimeUnit.SECONDS)
//                .subscribe(textChanged -> {
//                    MainActivity.this.runOnUiThread(new Runnable() {
//                        public void run() {
//                            rxText.setVisibility(View.GONE);
////                            rxText.setEnabled(false);
//                        }
//                    });
//
//
//                });

        btnShowImage = findViewById(R.id.btnShowImage);
        btnShowImage.setOnClickListener(v -> {

            byte[] decodedString = Base64.decode(strBase64, Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

            Glide.with(this)
                    .load(decodedByte)
                    .into(imageView);

            showString.setText(strBase64);

        });
        //btnSend = findViewById(R.id.btnSend);
        //txText.setEnabled(false);
        //btnSend.setEnabled(false);
        //txText = findViewById(R.id.txText);
        //rxText.setLayoutParams(new Constraints.LayoutParams(getScreenHeight(),getScreenWidth()));
       /* btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String data = txText.getText().toString();
                usbSerialConnector.writeAsync(data.getBytes());
            }
        });*/

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
////                rxText.setText(strBase64);
//                rxText.setVisibility(View.GONE);
//            }
//        }, 8000);

        findViewById(R.id.go).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rxText.setVisibility(View.GONE);

            }
        });

    }

    Bitmap textToBitmap(String text)
    {
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(12);
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        Bitmap bitmap = Bitmap.createBitmap(bounds.width(), bounds.height(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawText(text, 0, 0, paint);
        return bitmap;
    }

    @Override
    public void onDataReceived(final byte[] data) {
        strBase64 = strBase64 + Utilities.bytesToString(data);
//        rxText.setText(strBase64);
//        rxText.invalidate();
    }

    @Override
    public void onErrorReceived(String data) {
        Toast.makeText(this, data, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDeviceReady(ResponseStatus responseStatus) {
        rxText.setEnabled(true);
        // txText.setEnabled(true);
        //btnSend.setEnabled(true);
    }

    @Override
    public void onDeviceDisconnected() {
        Toast.makeText(this, "Device disconnected", Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        usbSerialConnector.setUsbSerialListener(this);
        usbSerialConnector.init(this, 115200);
    }

    @Override
    protected void onPause() {
        super.onPause();
        usbSerialConnector.pausedActivity();
    }

    private int getScreenHeight() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    private int getScreenWidth() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

}
